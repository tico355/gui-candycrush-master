<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client as GuzzleClient;
use Guzzle\Http\Message\Request as GuzzleRequest;
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password'];
    protected $table='users';
    public $remember_token = false;
    public $timestamps = false;
    private $urlApi = 'http://apicandycrush.apphb.com/api/';
    private $client;

    
    
    public function GetAllUsers(){
        $client = new GuzzleClient(['base_uri'=>$this->urlApi]);
        $response = $client->get('Users');
        $body = json_decode($response->getBody(),true);
        return $body;
    }


    public function GetUser($id)
    {
        $this->client = new GuzzleClient(['base_uri'=>$this->urlApi]);
        $response = $this->client->get('Users/'.$id);
        $body = json_decode($response->getBody(),true);
        return $body;
    }

    public function UpdateUser($request,$id)
    {
        $data = ['id'=> $request['id'],'name' => $request['name'],'email'=>$request['email'],'password'=>$request['password']];
        $this->client = new GuzzleClient(['base_uri'=>$this->urlApi]);
        $response = $this->client->put('Users/'.$id,['form_params'=>$data]);
        return $response->getStatusCode();
    }

    public function deleteUser($id)
    {
        $this->client = new GuzzleClient(['base_uri'=>$this->urlApi]);
        $response= $this->client->delete('Users/'.$id);
        return $response->getStatusCode();
    }

}
