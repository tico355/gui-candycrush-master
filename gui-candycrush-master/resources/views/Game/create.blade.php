@extends('layouts.app')
@section('content')
    <h2>Vertical (basic) form</h2>
    <label>Numero de Partida : </label><input type="text" id="id_partida">
    <label>Nombre del Jugador : </label><input type="text" id="name">

    <input type="hidden" id="id_usuario" value=" {{ Auth::user()->id }}">
    <!-- Game -->
    <div class="container body-content">

        <div id="container">

        </div>
        <footer>
            <p></p>
        </footer>
    </div>
    <!-- -->

    <form role="form" action="/game" method="POST">
        <div class="form-group" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <label for="score">Initial Score</label>
            <input id="score_board" type="number" class="form-control" name="score">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>

    <!-- candy game js -->
    <script type="text/javascript" src="{{ URL::asset('js/candy/candy.js') }}"></script>
    <!-- ajax -->
    <script type="text/javascript" src="{{ URL::asset('js/services/partida/create.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/services/partida_usuario/create.js') }}"></script>
    @endsection