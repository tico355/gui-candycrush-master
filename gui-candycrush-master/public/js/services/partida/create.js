/**
 * Created by John Benavides on 15/8/2016.
 */
//crea un var para guardar los datos que están en los inputs de la vista.
var dataDTO = {
    score: '0'
};
//llama al objeto Resource creado en el resource.js,
//llama al create y le pasamos el var donde guardamos los datos de la vista
//al .done es para devolver los datos despues de la llamada ajax y así agregarlos a la tabla con el .append
PartidaResource.create(dataDTO, false).done(function (partida) {
    document.getElementById('id_partida').value = partida.id;
    document.getElementById('score_board').value = partida.score;
    console.log(JSON.stringify(partida));
}).fail(function (error) {
    console.log(error);
    alert("Ha ocurrido un error");
});
