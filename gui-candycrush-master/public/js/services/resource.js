/**
 * Created by John Benavides on 15/8/2016.
 */
// objteto tipo Resource
var Resource = function (resource) {
    //variable que guarda el nombre del la entidad
    this.resource = resource;
    var apihost = 'http://apicandycrush.apphb.com/api/';
    //llamada ajax
    this.getAll = function () {
        return $.ajax({ url: apihost + this.resource  });
    };
    this.create = function (dataDTO, isAsync) {
        return $.ajax({
            type: "POST",
            async: isAsync,
            url: apihost + this.resource,
            data: dataDTO
        });
    };
    this.delete = function (id) {
        return $.ajax({
            type: "DELETE",
            url: apihost + this.resource + '/' + id
        });
    };
};

//creamos uno por cada entidad de objeto resource creado arriba que recibe el nombre de la entidad
var PartidaResource = new Resource('Partidas');
var UserPartidaResource = new Resource('UserPartidas');
